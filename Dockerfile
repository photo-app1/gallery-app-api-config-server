FROM openjdk:11
VOLUME /tmp
COPY apiEncryptionKey.jks apiEncryptionKey.jks
COPY UnlimitedJCEPolicyJDK8/* /usr/lib/jvm/java-11-openjdk/lib/security/
COPY build/libs/gallery-app-api-config-server-0.0.1-SNAPSHOT.jar GalleryAppApiConfigServer.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","GalleryAppApiConfigServer.jar"]