package com.mobileapp.photoappapiconfigserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

@SpringBootApplication
@EnableConfigServer
public class GalleryAppApiConfigServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(GalleryAppApiConfigServerApplication.class, args);
	}

}
